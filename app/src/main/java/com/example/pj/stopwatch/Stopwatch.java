package com.example.pj.stopwatch;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.TextView;
import android.widget.ToggleButton;

import java.util.ArrayList;
import java.util.List;


public class Stopwatch extends Activity {
    final Handler handler = new Handler();
    Runnable r = new Thread(){
        public void run(){
            runTimer();
        }
    };
    private int seconds = 0;
    private boolean running;
    private boolean warRunning;
    private String time;
    String history = "";

    /*
        Метод вызывается при удалении активности (поворот экрана), перед запуском метода onDelete().
        Для сохранения переменных.

     */
    @Override
    public void onSaveInstanceState(Bundle bundle){
        bundle.putInt("seconds", seconds);
        bundle.putBoolean("running", running);
//        bundle.putBoolean("waRunning", warRunning);
        bundle.putString("history", history);
    }

    /*
        Метод вызывается после жизненого цикла, для
     */
    @Override
    public void onPause(){
        super.onPause();
//        warRunning = running;
//        running = false;
    }

    // Метод продолжает работу активности при ее остановке
    @Override
    public void onResume(){
        super.onResume();
        if (warRunning){
            running = true;
        }
        TextView tv = (TextView) findViewById(R.id.history);
        tv.setText(history);
    }

    //Создание активности
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stopwatch);
        if (savedInstanceState != null){
            history = savedInstanceState.getString("history");
            seconds = savedInstanceState.getInt("seconds");
            running = savedInstanceState.getBoolean("running");
//            warRunning = savedInstanceState.getBoolean("warRunning");
        }
        runTimer();
    }

    // Вызывается при нажатии на тумблера
    public void onToggleButtonClicked(View view) {
        boolean b = ((ToggleButton) view).isChecked();
        if (b) {
            running = true;
        } else {
            running = false;
        }
    }

    // Вызывается при нажатии кнопки збросить
    public void onClickReset(View view) {
        running = false;
        seconds = 0;
    }

    // Вызывается при нажатии кнопки круг
    public void onClickAround(View view) {
        TextView tv = (TextView) findViewById(R.id.history);
        if (running) {
            history += "-" + time + "\n";
            tv.setText(history);
            seconds = 0;
        }
    }

    // Метод работы секундамера
    private void runTimer() {
        final TextView timeView = (TextView) findViewById(R.id.time_view);
        int hours = seconds / 3600;
        int minutes = (seconds % 3600) / 60;
        int secs = seconds % 60;
        time = String.format("%d:%02d:%02d",
                hours, minutes, secs);
        timeView.setText(time);
        if (running) {
            seconds++;
        }
        handler.postDelayed(r, 1000L);
    }

    // Метод очистки истории
    public void onClickClear(View view) {
        TextView tv = (TextView) findViewById(R.id.history);
        history = "";
        tv.setText(history);
    }


}
